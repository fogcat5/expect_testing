# OVERVIEW
  Testing the *expect* code that's used in the mystart startup script.  It tries to run bash and send it commands, then wait for the command to exit.  However, it seems to not match the code prompt, but just timeout after a long time instead.

## Tests
  This script will try to run bash and send a few commands to see how this should work.  If all goes well, the startup scripts should run pretty quickly.


